/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../MdtCablingJsonDumpAlg.h"
#include "../MuonABLineJsonDumpAlg.h"
#include "../MdtAsBuiltJsonDumpAlg.h"
#include "../MdtCondJsonDumpAlg.h"
#include "../MdtCondJsonDumpAlg.h"
#include "../RpcToyCablingJsonDumpAlg.h"

DECLARE_COMPONENT(MdtCablingJsonDumpAlg)
DECLARE_COMPONENT(MuonABLineJsonDumpAlg)
DECLARE_COMPONENT(MdtAsBuiltJsonDumpAlg)
DECLARE_COMPONENT(MdtCondJsonDumpAlg)
DECLARE_COMPONENT(RpcToyCablingJsonDumpAlg)
